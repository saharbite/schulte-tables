## Таблицы Шульте для планшетов Android.
Сподручное приложение для тренировки навыка переферийного зрения.

Приложение доступно на [Itch.io](https://saharbite.itch.io/schulte-tables)

### Реализуемая дизайн-документация
- [Сподручные Таблицы Шульте](https://codeberg.org/saharbite/schulte-tables)


### Сборка проекта
Проект разработан на игровом движке Godot Engine 4.1.1.
Экспорт произведен на стандартном шаблоне экспорта для Андроид.


### Лицензия
```
    Handy Schulte Tables for android
    Copyright (C) 2024 Saharbite

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
```
