extends Panel

@onready var group = get_node("HBoxContainer/CenterContainer/Group")
@onready var number = get_node("HBoxContainer/CenterContainer2/Number")
@onready var time = get_node("HBoxContainer/CenterContainer3/Time")

func set_group(to_txt):
	group.set_text(str(to_txt))

func set_number(to_txt):
	number.set_text(str(to_txt))

func set_time(to_txt):
	time.set_text(str(to_txt))
