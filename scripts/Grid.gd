extends GridContainer

@onready var cell = preload("res://scenes/button.tscn")
@onready var grid = get_node(".")

var sled1 = 0 # Очередность группы
var sled2 = 0 # Очередность нажатий, для ведения статистики
var dict: Dictionary # Шаблон для заполения экрана клетками
var group_arr: Array # Рандомизация групп
var shablon: Dictionary # Шаблон сверки правильного ввода игроком  


var session_start = false
var session_time = 0.0
var poisk_start = false
var poisk_time = 0.0
var session_stat = 0
var hit_stat: Dictionary # Пример заполнения -- 1(номер_нажатия): [время_нажатия, цвет_группы, значение_кнопки]

var grid_size
var tasovka_check
var povorot_check
var vrashenie_check
var orient_check
var indikacia_check
var vcorrect_check

signal finished
signal warningtime_start
signal warningtime_pause


func fasovka(cell_size, grid_size_request, groups, schet, tasovka_request, povorot_request, vrashenie_request, orient_request, indikacia_request, vcorrect_request):
	tasovka_check = tasovka_request
	grid_size = grid_size_request
	povorot_check = povorot_request
	vrashenie_check = vrashenie_request
	orient_check = orient_request
	indikacia_check = indikacia_request
	vcorrect_check = vcorrect_request
	
	grid.set_columns(grid_size_request)
	
	ishodnik(groups)
	
	shablon_sverki(grid_size, groups, schet)
	
	peremeshka()
	
	# Добавление и настройка клеток 
	for x in grid_size ** 2:
		var instance = cell.instantiate()
		
		instance.set_custom_minimum_size(cell_size)
		add_child(instance)
		
		if groups == 1:
			instance.set_txt(dict["Red"].pop_back())
#			if x == grid_size ** 2 / 2: # кусок кода для создание иконки
#				instance.set_txt("0‿0")
		elif groups == 2:
			if group_arr.back() == "Red":
				instance.set_txt(dict["Red"].pop_back())
				instance.set_color("Red")
			else:
				instance.set_txt(dict["Green"].pop_back())
				instance.set_color("Green")
			group_arr.pop_back() 
		elif groups == 3:
			if group_arr.back() == "Red":
				instance.set_txt(dict["Red"].pop_back())
				instance.set_color("Red")
			elif group_arr.back() == "Green":
				instance.set_txt(dict["Green"].pop_back())
				instance.set_color("Green")
			else:
				instance.set_txt(dict["Blue"].pop_back())
				instance.set_color("Blue")
			group_arr.pop_back() 
		elif groups == 4:
			if group_arr.back() == "Red":
				instance.set_txt(dict["Red"].pop_back())
				instance.set_color("Red")
			elif group_arr.back() == "Green":
				instance.set_txt(dict["Green"].pop_back())
				instance.set_color("Green")
			elif group_arr.back() == "Blue":
				instance.set_txt(dict["Blue"].pop_back())
				instance.set_color("Blue")
			else:
				instance.set_txt(dict["Yellow"].pop_back())
				instance.set_color("Yellow")
			group_arr.pop_back() 
	
	if povorot_check == true:
		povorot(grid_size)
	
	if vrashenie_check == true:
		for x in grid_size ** 2:
			var btn = grid.get_child(x)
			btn.set_random_rotation(true)
	
	_draw()

# Для отрисовки поверх нужен отдельный узел
func _draw():
	if orient_check == true:
		
		draw_circle(Vector2(630 + grid_size - 1, 340 + grid_size - 1), 37, Color.WHITE_SMOKE) 
		draw_circle(Vector2(630 + grid_size - 1, 340 + grid_size - 1), 35, Color.BLACK) #633 345
		

# Создание, заполнение и рандомизация словарей
func ishodnik(groups):
	if groups == 1:
		dict["Red"] = []
		for x in grid_size ** 2:
			x += 1 
			dict["Red"].append(x)
	elif groups == 2:
		dict["Red"] = []
		dict["Green"] = []
		for x in grid_size ** 2 / 2:
			x += 1 
			dict["Red"].append(x)
			dict["Green"].append(x)
			group_arr.append_array(["Red", "Green"])
	elif groups == 3:
		dict["Red"] = []
		dict["Green"] = []
		dict["Blue"] = []
		for x in grid_size ** 2 / 3:
			x += 1 
			dict["Red"].append(x)
			dict["Green"].append(x)
			dict["Blue"].append(x)
			group_arr.append_array(["Red", "Green", "Blue"])
	elif groups == 4:
		dict["Red"] = []
		dict["Green"] = []
		dict["Blue"] = []
		dict["Yellow"] = []
		for x in grid_size ** 2 / 4:
			x += 1 
			dict["Red"].append(x)
			dict["Green"].append(x)
			dict["Blue"].append(x)
			dict["Yellow"].append(x)
			group_arr.append_array(["Red", "Green", "Blue", "Yellow"])

# Функция рандомизация последовательности словаря
func shablon_sverki(grid_size, groups, schet):
	
	# Если кол-во клеток не четное(полное) для групп, 
	# 1-вая группа словаря получает доп. число
	if grid_size ** 2 % groups != 0:
		dict["Red"].append(dict["Red"].max() + 1)
		group_arr.append("Red")
	
	group_arr.shuffle()
	shablon = dict.duplicate(true)
	
	# Создание шаблона словаря для указанного порядка счета
	if schet == 1: # Последовательный
		# Оптимизация словаря 
		for x in shablon.size():
			shablon[shablon.keys()[x]].reverse()
		#print(shablon)
	elif schet == 2: # Обратный
		#print(shablon)
		pass
	else: # Расходящийся
		#print(shablon)
		var mesto1: int # Позиция вводимая в массив
		var mesto2: int # Забираемая позиция из массива
		for x in shablon.size():
			var sled3: int
			var sled4: int
			for y in shablon.values()[x].size() / 2 - 1:
				mesto1 = shablon.values()[x].size() / 2 + sled3 
				mesto2 = shablon.values()[x].size() / 2 - 2 - sled4 
				shablon[shablon.keys()[x]].insert(mesto1, shablon[shablon.keys()[x]].pop_at(mesto2))
				sled3 += 1
				sled4 += 1
		
		for x in shablon.size():
			shablon[shablon.keys()[x]].reverse()
		#print(shablon)

func peremeshka():
	for x in dict.size():
		dict[dict.keys()[x % dict.size()]].shuffle()


func chistka():
	dict.clear()
	shablon.clear()
	group_arr.clear()
	sled1 = 0
	sled2 = 0
	session_start = false
	session_time = 0.0
	poisk_start = false
	poisk_time = 0.0
	session_stat = 0
	hit_stat = {}
	for button in grid.get_children():
		remove_child(button)
		button.queue_free()


func podhvat():
	for button in grid.get_children():
		button.pressed.connect(_on_pressed.bind(button))

func _on_pressed(button):
	if shablon.size() == 1:
		if button.value == shablon["Red"].back():
			schet_success(button)
		else:
			schet_fail(button)
		if shablon["Red"].is_empty() == true:
			schet_end()
	
	if shablon.size() == 2:
		if sled1 == 0 and button.color == "Red" and button.value == shablon["Red"].back():
			schet_success(button)
			sled1 += 1
		elif sled1 == 1 and button.color == "Green" and button.value == shablon["Green"].back():
			schet_success(button)
			sled1 -= 1
		else:
			schet_fail(button)
		if shablon["Red"].is_empty() and shablon["Green"].is_empty() == true:
			schet_end()
	
	if shablon.size() == 3:
		if sled1 == 0 and button.color == "Red" and button.value == shablon["Red"].back():
			schet_success(button)
			sled1 += 1
		elif sled1 == 1 and button.color == "Green" and button.value == shablon["Green"].back():
			schet_success(button)
			sled1 += 1
		elif sled1 == 2 and button.color == "Blue" and button.value == shablon["Blue"].back():
			schet_success(button)
			sled1 -= 2
		else:
			schet_fail(button)
		if shablon["Red"].is_empty() and shablon["Green"].is_empty() and shablon["Blue"].is_empty() == true:
			schet_end()
	
	if shablon.size() == 4:
		if sled1 == 0 and button.color == "Red" and button.value == shablon["Red"].back():
			schet_success(button)
			sled1 += 1
		elif sled1 == 1 and button.color == "Green" and button.value == shablon["Green"].back():
			schet_success(button)
			sled1 += 1
		elif sled1 == 2 and button.color == "Blue" and button.value == shablon["Blue"].back():
			schet_success(button)
			sled1 += 1
		elif sled1 == 3 and button.color == "Yellow" and button.value == shablon["Yellow"].back():
			schet_success(button)
			sled1 -= 3
		else:
			schet_fail(button)
		if shablon["Red"].is_empty() and shablon["Green"].is_empty() and shablon["Blue"].is_empty() and shablon["Yellow"].is_empty() == true:
			schet_end()


func schet_success(button):
	schet_start()
	
	if button.color == null:
		shablon["Red"].pop_back()
	else:
		shablon[button.color].pop_back()
	
	hit_stat[sled2] = [get_poisk_time(), button.get_color(), button.value]
	reset_poisk_time()
	sled2 += 1
	
	if tasovka_check == true:
		tasovka(grid_size)
		if povorot_check == true:
			povorot(grid_size)
	
	if vcorrect_check == true:
		button.rotation_check = false
		button.button_pressed = false
		button.correct()
		await get_tree().create_timer(0.9).timeout
		if indikacia_check == true:
			button.button_pressed = true
	else:
		button.rotation_check = false
		if indikacia_check == false:
			button.button_pressed = false


func schet_fail(button):
	hit_stat[sled2] = ["промах", button.get_color(), button.value]
	sled2 += 1
	
	var inc_prs
	if button.button_pressed == false:
		inc_prs = false
	else:
		inc_prs = true
	
	if vcorrect_check == true:
		button.button_pressed = false
		button.incorrect()
		await get_tree().create_timer(0.9).timeout
		if indikacia_check == true and inc_prs == false:
			button.button_pressed = true
		else:
			button.button_pressed = false
	else:
		if indikacia_check == true and inc_prs == false:
			button.button_pressed = true
		else:
			button.button_pressed = false


func schet_start():
	session_start = true
	poisk_start = true
	warningtime_start.emit()

func schet_end():
	session_start = false
	session_stat = get_session_time()
	finished.emit()
	warningtime_pause.emit()


func tasovka(grid_size):
	for x in grid_size ** 2:
		grid.move_child(grid.get_child(0), randi() % grid_size ** 2)

func povorot(grid_size):
	for x in grid_size ** 2:
		var sug = grid.get_child(x)
		sug.set_random_angle(randi_range(1, 4))


func get_session_time():
	return session_time

func get_poisk_time():
	return poisk_time

func reset_poisk_time():
	poisk_time = 0.0


func _process(delta):
	if session_start == true:
		session_time += delta
	if poisk_start == true:
		poisk_time += delta
