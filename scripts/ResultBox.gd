extends Panel

@onready var stroka = preload("res://scenes/stroka.tscn")

@onready var time = get_node("VBoxContainer/Panel/HBoxContainer/LabelTime")
@onready var press_correct = get_node("VBoxContainer/Panel2/HBoxContainer/LabelNumber")
@onready var press_false = get_node("VBoxContainer/Panel3/HBoxContainer/LabelNumber")
@onready var pressed_stat = get_node("PressedButtons/VBoxContainer")


func load_results(session_stat, hit_stat):
	var cpn = 0 # Correct_Press_Number
	var fpn = 0 # False_Press_Number
	
	for x in hit_stat.size():
		var instance = stroka.instantiate()
		pressed_stat.add_child(instance)
		if hit_stat.values()[x].front() is String:
			fpn += 1
			instance.set_time("промах")
		else:
			cpn += 1
			instance.set_time("%5.2f" % hit_stat.values()[x].front() + "с")
		instance.set_group(hit_stat.values()[x][1])
		instance.set_number(hit_stat.values()[x][2])
		
	press_correct.set_text(str(cpn))
	press_false.set_text(str(fpn))
	
	# переводим секунды в минуты - 02:03.05
	var a = int(session_stat) / 60
	var b = session_stat - a * 60
	var c: String
	if a < 10: 
		c = "0" + str(a) + ":"
	elif a > 10:
		c = str(a)
	if b < 10:
		c += "0" + "%2.2f" % b
	elif b > 10:
		c += "%2.2f" % b
	time.set_text(c)
	
func chistka():
	for stroka in pressed_stat.get_children():
		pressed_stat.remove_child(stroka)
		stroka.queue_free()
