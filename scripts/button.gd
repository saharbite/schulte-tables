extends Button

@onready var label = get_node("Label")
@onready var anim = $AnimationPlayer

var value
var color
var rotation_check = false
var rotation_speed = 2
var rotation_direction = 0
var random_direction = [-1, 1]



func set_txt(to_txt):
	value = to_txt
	label.set_text(str(to_txt))

func set_color(to_color):
	color = to_color
	if to_color == "Red":
		label.modulate = Color(0.9, 0.1, 0.1, 1)
	elif to_color == "Green":
		label.modulate = Color(0.1, 0.9, 0.1, 1)
	elif to_color == "Blue":
		label.modulate = Color(0.1, 0.9, 0.9, 1)
	elif to_color == "Yellow":
		label.modulate = Color(1, 0.85, 0, 1)

func set_random_angle(angle):
	if angle == 1:
		label.set_rotation_degrees(0)
	elif angle == 2:
		label.set_rotation_degrees(90)
	elif angle == 3:
		label.set_rotation_degrees(180)
	else:
		label.set_rotation_degrees(270)

func set_random_rotation(check):
	if check == true:
		rotation_check = true
		rotation_direction = random_direction.pick_random()

func get_color():
	if color == "Red":
		return "Красные"
	elif color == "Green":
		return "Зеленые"
	elif color == "Blue":
		return "Синии"
	elif color == "Yellow":
		return "Желтые"
	else:
		return "Белые"

func correct():
	anim.play("Зеленый сигнал")
	
func incorrect():
	anim.play("Красный сигнал")

func _process(delta):
	if rotation_check == true:
		label.rotation += rotation_direction * rotation_speed * delta 

