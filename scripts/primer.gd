extends Panel

@onready var label = get_node("Label")

func set_txt(to_txt):
	label.set_text(str(to_txt))

func set_color(to_color):
	if to_color == "Red":
		label.modulate = Color(0.9, 0.1, 0.1, 1)
	elif to_color == "Green":
		label.modulate = Color(0.1, 0.9, 0.1, 1)
	elif to_color == "Blue":
		label.modulate = Color(0.1, 0.9, 0.9, 1)
	elif to_color == "Yellow":
		label.modulate = Color(1, 0.85, 0, 1)
