extends Panel

@onready var primer = preload("res://scenes/primer.tscn")
@onready var cntprimer = $ContainPrimer
@onready var set1 = $ButtonSet1
@onready var set2 = $ButtonSet2
@onready var set3 = $ButtonSet3

var grid_size
var groups
var schet


var dict: Dictionary
var shablon: Dictionary
var primers: Array
var saved_sets: Array = [[], [], []]

const colors: Array = ["Red", "Green", "Blue", "Yellow"]

signal save_tip
signal load_tip

# Функции для работы предпросмотра способа счета
func _ready():
	grid_size = $OptionTable.get_selected_id()
	groups = $OptionGroup.get_selected_id()
	schet = $OptionSchet.get_selected_id()
	ishodnik(groups, grid_size)
	shablon_sverki(grid_size, groups, schet)
	make_primer()
	load_primer()
	if FileAccess.file_exists("user://savedsets.data") == true:
		var file = FileAccess.open("user://savedsets.data", FileAccess.READ)
#		var save = file.get_var()
		saved_sets.clear()
		saved_sets = file.get_var().duplicate(true)
		file.close()
func _on_option_table_item_selected(index):
	grid_size = index + 3
	ishodnik(groups, grid_size)
	shablon_sverki(grid_size, groups, schet)
	make_primer()
	load_primer()
func _on_option_group_item_selected(index):
	groups = index + 1
	ishodnik(groups, grid_size)
	shablon_sverki(grid_size, groups, schet)
	make_primer()
	load_primer()
func _on_option_schet_item_selected(index):
	schet = index + 1
	ishodnik(groups, grid_size)
	shablon_sverki(grid_size, groups, schet)
	make_primer()
	load_primer()


func make_primer():
	for x in shablon.size():
		if schet == 1:
			var format_string = "%s → %s" % [shablon.values()[x][-1], shablon.values()[x][0]]
			primers.append(format_string)
		elif schet == 2:
			var format_string = "%s → %s" % [shablon.values()[x][-1], shablon.values()[x][0]]
			primers.append(format_string)
		else:
			var format_string = "← %s | %s →" % [shablon.values()[x][-1], shablon.values()[x][-2]]
			primers.append(format_string)


# Вытаскиваем сцены, настраиваем их строки и цвет
func load_primer():
	for x in shablon.size():
		var instance = primer.instantiate()
		cntprimer.add_child(instance)
		if groups == 1:
			instance.set_txt(primers[x])
		else:
			instance.set_txt(primers[x])
			instance.set_color(colors[x])


# Создание, заполнение и рандомизация словарей
func ishodnik(groups, grid_size):
	chistka()
	if groups == 1:
		dict["Red"] = []
		for x in grid_size ** 2:
			x += 1 
			dict["Red"].append(x)
	elif groups == 2:
		dict["Red"] = []
		dict["Green"] = []
		for x in grid_size ** 2 / 2:
			x += 1 
			dict["Red"].append(x)
			dict["Green"].append(x)
	elif groups == 3:
		dict["Red"] = []
		dict["Green"] = []
		dict["Blue"] = []
		for x in grid_size ** 2 / 3:
			x += 1 
			dict["Red"].append(x)
			dict["Green"].append(x)
			dict["Blue"].append(x)
	elif groups == 4:
		dict["Red"] = []
		dict["Green"] = []
		dict["Blue"] = []
		dict["Yellow"] = []
		for x in grid_size ** 2 / 4:
			x += 1 
			dict["Red"].append(x)
			dict["Green"].append(x)
			dict["Blue"].append(x)
			dict["Yellow"].append(x)

# Функция рандомизация последовательности словаря
func shablon_sverki(grid_size, groups, schet):
	
	# Если кол-во клеток не четное(полное) для групп, 
	# 1-вая группа словаря получает доп. число
	if grid_size ** 2 % groups != 0:
		dict["Red"].append(dict["Red"].max() + 1)


	# Создание шаблона словаря для указанного порядка счета
	if schet == 1: # Последовательный
		shablon = dict.duplicate(true)
		# Оптимизация словаря 
		for x in shablon.size():
			shablon[shablon.keys()[x]].reverse()
		#print(shablon)
	elif schet == 2: # Обратный
		shablon = dict.duplicate(true)
		#print(shablon)
	else: # Расходящийся
		shablon = dict.duplicate(true)
		
		var mesto1: int # Позиция вводимая в массив
		var mesto2: int # Забираемая позиция из массива
		for x in shablon.size():
			var sled3: int
			var sled4: int
			for y in shablon.values()[x].size() / 2 - 1:
				mesto1 = shablon.values()[x].size() / 2 + sled3 
				mesto2 = shablon.values()[x].size() / 2 - 2 - sled4 
				shablon[shablon.keys()[x]].insert(mesto1, shablon[shablon.keys()[x]].pop_at(mesto2))
				sled3 += 1
				sled4 += 1
		
		for x in shablon.size():
			shablon[shablon.keys()[x]].reverse()
		#print(shablon)


func chistka():
	dict.clear()
	shablon.clear()
	primers.clear()
	for primer in cntprimer.get_children():
		cntprimer.remove_child(primer)
		primer.queue_free()


func _on_button_set_1_button_down():
	await get_tree().create_timer(1.3).timeout
	if set1.get_draw_mode() == 1:
		save_set(1)
	else:
		load_set(1)
		$ButtonSet1.set_text("●")

func _on_button_set_2_button_down():
	await get_tree().create_timer(1.3).timeout
	if set2.get_draw_mode() == 1:
		save_set(2)
	else:
		load_set(2)
		$ButtonSet2.set_text("●")

func _on_button_set_3_button_down():
	await get_tree().create_timer(1.3).timeout
	if set3.get_draw_mode() == 1:
		save_set(3)
	else:
		load_set(3)
		$ButtonSet3.set_text("●")


func save_set(set_number):
	var set: Array = [
		-3 + $OptionTable.get_selected_id(),
		-1 + $OptionGroup.get_selected_id(),
		-1 + $OptionSchet.get_selected_id(),
		$CheckPeretasovka.is_pressed(),
		$CheckPovorot.is_pressed(),
		$CheckVrashenie.is_pressed(),
		$CheckOrient.is_pressed(),
		$CheckIndikacia.is_pressed(),
		$CheckVСorrect.is_pressed()
	]
	print(set)
	
	if set_number == 1:
		saved_sets[0].clear()
		saved_sets[0] = set.duplicate()
	elif set_number == 2:
		saved_sets[1].clear()
		saved_sets[1] = set.duplicate()
	else:
		saved_sets[2].clear()
		saved_sets[2] = set.duplicate()
	
#	print(saved_sets.set1, saved_sets.set2, saved_sets.set3)
	
	var file = FileAccess.open("user://savedsets.data", FileAccess.WRITE)
	file.store_var(saved_sets)
	file.close()
	save_tip.emit()

func load_set(set_number):
	if set_number == 1:
		if saved_sets[0].is_empty() == false:
			$OptionTable.select(saved_sets[0][0])
			$OptionGroup.select(saved_sets[0][1])
			$OptionSchet.select(saved_sets[0][2])
			$CheckPeretasovka.set_pressed(saved_sets[0][3])
			$CheckPovorot.set_pressed(saved_sets[0][4])
			$CheckVrashenie.set_pressed(saved_sets[0][5])
			$CheckOrient.set_pressed(saved_sets[0][6])
			$CheckIndikacia.set_pressed(saved_sets[0][7])
			$CheckVСorrect.set_pressed(saved_sets[0][8])
			grid_size = $OptionTable.get_selected_id()
			groups = $OptionGroup.get_selected_id()
			schet = $OptionSchet.get_selected_id()
			ishodnik(groups, grid_size)
			shablon_sverki(grid_size, groups, schet)
			make_primer()
			load_primer()
			load_tip.emit()
		else:
			$ButtonSetTip.set_pressed(true)
	elif set_number == 2:
		if saved_sets[0].is_empty() == false:
			$OptionTable.select(saved_sets[1][0])
			$OptionGroup.select(saved_sets[1][1])
			$OptionSchet.select(saved_sets[1][2])
			$CheckPeretasovka.set_pressed(saved_sets[1][3])
			$CheckPovorot.set_pressed(saved_sets[1][4])
			$CheckVrashenie.set_pressed(saved_sets[1][5])
			$CheckOrient.set_pressed(saved_sets[1][6])
			$CheckIndikacia.set_pressed(saved_sets[1][7])
			$CheckVСorrect.set_pressed(saved_sets[1][8])
			grid_size = $OptionTable.get_selected_id()
			groups = $OptionGroup.get_selected_id()
			schet = $OptionSchet.get_selected_id()
			ishodnik(groups, grid_size)
			shablon_sverki(grid_size, groups, schet)
			make_primer()
			load_primer()
			load_tip.emit()
		else:
			$ButtonSetTip.set_pressed(true)
	else:
		if saved_sets[0].is_empty() == false:
			$OptionTable.select(saved_sets[2][0])
			$OptionGroup.select(saved_sets[2][1])
			$OptionSchet.select(saved_sets[2][2])
			$CheckPeretasovka.set_pressed(saved_sets[2][3])
			$CheckPovorot.set_pressed(saved_sets[2][4])
			$CheckVrashenie.set_pressed(saved_sets[2][5])
			$CheckOrient.set_pressed(saved_sets[2][6])
			$CheckIndikacia.set_pressed(saved_sets[2][7])
			$CheckVСorrect.set_pressed(saved_sets[2][8])
			grid_size = $OptionTable.get_selected_id()
			groups = $OptionGroup.get_selected_id()
			schet = $OptionSchet.get_selected_id()
			ishodnik(groups, grid_size)
			shablon_sverki(grid_size, groups, schet)
			make_primer()
			load_primer()
			load_tip.emit()
		else:
			$ButtonSetTip.set_pressed(true)
			
