extends Panel

@onready var source = $LinkSource
@onready var news = $LinkNews
@onready var textbox = $TextAbout
@onready var gpl = $LinkGPL

func _on_tab_bar_tab_clicked(tab): # отсылает занчения от 0 до 2
	if tab == 0:
		textbox.set_text("Таблицы Шульте для планшетов Android.\nСподручное приложение для тренировки навыка переферийного зрения.\n\n\n\nВерсия 1.23")
		news.visible = true
		source.visible = true
		gpl.visible = false
		news.set_process_mode(0)
		source.set_process_mode(0)
		gpl.set_process_mode(4)
	elif tab == 1:
		textbox.set_text("Сопровождение и разработка\n@Saharbite")
		news.visible = false
		source.visible = false
		gpl.visible = false
		news.set_process_mode(4)
		source.set_process_mode(4)
		gpl.set_process_mode(4)
	elif tab == 2:
		textbox.set_text("Данная программа распространяется под лицензией \n\nКомпоненты:\n• Godot Engine v4.1.1 - MIT\n• Шрифт Inter - Open Font License\n• Шрифт Wix Madefor Text - Open Font License")
		news.visible = false
		source.visible = false
		gpl.visible = true
		news.set_process_mode(4)
		source.set_process_mode(4)
		gpl.set_process_mode(0)

