extends Node

@onready var operator = get_node("CenterCubes/Grid")
@onready var tutor = get_node("CenterTutor/Tutor")
@onready var warning_timer = get_node("CenterWarning/Timer")
@onready var result = get_node("CenterResult/ResultBox")
@onready var tip = get_node("CenterTip/Panel")

@onready var anim_grid = $CenterCubes/AnimationPlayer
@onready var anim_menu = $CenterMenu/Menu/AnimationPlayer
@onready var anim_tutor = $CenterTutor/Tutor/AnimationPlayer
@onready var anim_result = $CenterResult/ResultBox/AnimationPlayer
@onready var anim_warning = $CenterWarning/Panel/AnimationPlayer
@onready var anim_about = $CenterAbout/AboutBox/AnimationPlayer
@onready var anim_tip = $CenterTip/Panel/AnimationPlayer


var screen_width = 1260 #1260, 800
var screen_height = 680
var cell_size: Vector2
var save_dict: Dictionary

var grid_size
var groups
var schet

var tasovka_check
var povorot_check
var vrashenie_check
var orient_check
var indikacia_check
var vcorrect_check

var tutor_page


func _ready():
	if FileAccess.file_exists("user://savegame.data") == false:
		_on_accept_button_pressed()
	else:
		load_savefile()
#		_on_accept_button_pressed()

# Функция применения выбраных настроек
func _on_accept_button_pressed():
	operator.chistka()
	
	grid_size = $CenterMenu/Menu/OptionTable.get_selected_id()
	groups = $CenterMenu/Menu/OptionGroup.get_selected_id()
	schet = $CenterMenu/Menu/OptionSchet.get_selected_id()
	tasovka_check = $CenterMenu/Menu/CheckPeretasovka.is_pressed()
	povorot_check = $CenterMenu/Menu/CheckPovorot.is_pressed()
	vrashenie_check = $CenterMenu/Menu/CheckVrashenie.is_pressed()
	orient_check = $CenterMenu/Menu/CheckOrient.is_pressed()
	indikacia_check = $CenterMenu/Menu/CheckIndikacia.is_pressed()
	vcorrect_check = $CenterMenu/Menu/CheckVСorrect.is_pressed()
	cell_size = narezka(screen_width, screen_height, grid_size)
	
	make_save()
	
	operator.fasovka(cell_size, 
		grid_size, 
		groups, 
		schet, 
		tasovka_check, 
		povorot_check, 
		vrashenie_check,
		orient_check, 
		indikacia_check,
		vcorrect_check, 
		)
	operator.podhvat()
	
	await get_tree().create_timer(0.2).timeout
	$Button.button_pressed = false

func narezka(a, b, c):
	var d = Vector2(a / c, b / c)
	return d


func make_save():
	save_dict = {
		"grid_size": grid_size, 
		"groups": groups, 
		"schet": schet, 
		"tasovka_check": tasovka_check, 
		"povorot_check": povorot_check,
		"vrashenie_check": vrashenie_check,
		"orient_check": orient_check,
		"indikacia_check": indikacia_check,
		"vcorrect_check": vcorrect_check,
		"cell_size": cell_size
		}
	var file = FileAccess.open("user://savegame.data", FileAccess.WRITE)
	file.store_var(save_dict)
	file.close()

func load_savefile():
	operator.chistka()
	
	var file = FileAccess.open("user://savegame.data", FileAccess.READ)
	save_dict.merge(file.get_var(), true)
	file.close()
	
	operator.fasovka(save_dict["cell_size"],
		save_dict["grid_size"], 
		save_dict["groups"], 
		save_dict["schet"], 
		save_dict["tasovka_check"], 
		save_dict["povorot_check"], 
		save_dict["vrashenie_check"], 
		save_dict["orient_check"], 
		save_dict["indikacia_check"],
		save_dict["vcorrect_check"]
		)
	operator.podhvat()
	
	await get_tree().create_timer(0.2).timeout
	$Button.button_pressed = false


# Функции для работы меню руководства, результатов и предупреждений
func _on_manual_button_pressed():
	tutor_page = 0
	tutor.set_text(tutor_page)
	anim_menu.play("Исчезание окна")
	$Button.visible = false
	$Button.set_process_mode(4)
	$CenterTutor.visible = true
	anim_tutor.play("windows/Вход окна")
func _on_button_back_pressed():
	if tutor_page == 0:
		anim_tutor.play_backwards("windows/Вход окна")
		await get_tree().create_timer(0.2).timeout
		$Button.set_process_mode(0)
		$Button.visible = true
		$CenterTutor.visible = false
		anim_menu.play("Появление окна")
	else:
		tutor_page -= 1
		tutor.set_text(tutor_page)
func _on_button_frw_pressed():
	if tutor_page == 2:
		anim_tutor.play_backwards("windows/Вход окна")
		await get_tree().create_timer(0.2).timeout
		$CenterTutor.visible = false
		anim_menu.play("Появление окна")
		tutor_page = 0
		$Button.set_process_mode(0)
		$Button.visible = true
	else:
		tutor_page += 1
		tutor.set_text(tutor_page)

func _on_grid_finished():
	result.load_results(operator.session_stat, operator.hit_stat)
	anim_grid.play("Исчезание окна")
	$Button.visible = false
	$Button.set_process_mode(4)
	$CenterResult.visible = true
	anim_result.play("windows/Вход окна")
func _on_button_restart_pressed():
	anim_result.play_backwards("windows/Вход окна")
	await get_tree().create_timer(0.2).timeout
	$CenterResult.visible = false
	_on_accept_button_pressed()
	anim_grid.play("Появление окна")
	$Button.visible = true
	$Button.set_process_mode(0)
	result.chistka()
func _on_button_setting_pressed():
	anim_result.play_backwards("windows/Вход окна")
	await get_tree().create_timer(0.2).timeout
	$CenterResult.visible = false
	$CenterCubes.visible = false
	$Button.set_process_mode(0)
	$Button.button_pressed = true
	$Button.visible = true
	result.chistka()

func _on_grid_warningtime_start():
	warning_timer.start()
	if warning_timer.is_paused() == true:
		warning_timer.set_paused(false)
func _on_grid_warningtime_pause():
	warning_timer.set_paused(true)
func _on_timer_timeout():
	anim_grid.play("Исчезание окна")
	$Button.visible = false
	$Button.set_process_mode(4)
	$CenterWarning.visible = true
	anim_warning.play("windows/Вход окна")
func _on_close_warning_pressed():
	$CenterWarning.visible = false
	anim_warning.play_backwards("windows/Вход окна")
	anim_grid.play("Появление окна")
	$Button.visible = true
	$Button.set_process_mode(0)

func _on_about_pressed():
	anim_menu.play("Исчезание окна")
	$Button.visible = false
	$Button.set_process_mode(4)
	$CenterAbout.visible = true
	anim_about.play("windows/Вход окна")
func _on_close_about_pressed():
	$Button.set_process_mode(0)
	$Button.visible = true
	anim_about.play_backwards("windows/Вход окна")
	await get_tree().create_timer(0.2).timeout
	anim_menu.play("Появление окна")
	$CenterAbout.visible = false

# Вызов меню настроек
func _on_button_toggled(button_pressed):
	if button_pressed == true:
		anim_grid.play("Исчезание окна")
		await get_tree().create_timer(0.1).timeout
		anim_menu.play("Вход окна")
	else: 
		$CenterCubes.visible = true
		anim_menu.play_backwards("Вход окна")
		await get_tree().create_timer(0.1).timeout
		anim_grid.play("Появление окна")


func _on_button_set_tip_pressed():
	$CenterTip.visible = true
	$CenterTip/Panel/Label.set_text("Чтобы сохранить выбранные настройки: \n• Зажмите и удерживайте любой из \n   слотов предустановки.")
	anim_tip.play("Вход подсказки")
	await get_tree().create_timer(6).timeout
	anim_tip.play_backwards("Вход подсказки")
	await get_tree().create_timer(1.2).timeout
	$CenterTip.visible = false

func _on_menu_load_tip():
	$CenterTip.visible = true
	$CenterTip/Panel/Label.set_text("Настройка загружена")
	anim_tip.play("Вход подсказки")
	await get_tree().create_timer(2).timeout
	anim_tip.play_backwards("Вход подсказки")
	await get_tree().create_timer(1.2).timeout
	$CenterTip.visible = false

func _on_menu_save_tip():
	$CenterTip.visible = true
	$CenterTip/Panel/Label.set_text("Выбранная настройка сохранена")
	anim_tip.play("Вход подсказки")
	await get_tree().create_timer(2).timeout
	anim_tip.play_backwards("Вход подсказки")
	await get_tree().create_timer(1.2).timeout
	$CenterTip.visible = false
